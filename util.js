import { writeFile } from 'fs';
import db from './db.js';

/**
 * Write given value to the file with the specified
 *
 * @param {string} path
 * @param {*} value
 */
export const writeToFile = (path, value) =>
  new Promise(resolve => writeFile(path, value, () => resolve()));

/**
 * Print the current state of the DB.
 */
export const printDbState = () => {
  for (let el in db) {
    // for arrays print their length
    if (Array.isArray(db[el])) {
      console.log(el, ':', db[el].length);
    } else if (typeof db[el] === 'object') {
      // for maps just print their size
      console.log(el, ':', db[el].size);
    } else {
      console.log(el, ':', db[el]);
    }
  }
};

/**
 * Scan books from a library.
 * 
 * @param {Library} library 
 */
export const scanBooks = library => {
  // loop as many times as many books per day we can scan from the library
  for (let i = 0; i < library.maxBooksPerDay; i++) {
  // if library has any books left
    if (library.books.size !== 0) {
      // get the first (highest score because they are sorted) book and delete it from the library
      const [index, book] = library.books.entries().next().value;
      library.books.delete(index);

      // if the books has not already been picked
      if (!db.alreadyPickedIds.includes(index)) {
        // if the output map already has the library, append the book
        if (db.pickedBooks.has(library)) {
          db.pickedBooks.get(library).push(book);
        } else {
          // otherwise add entry
          db.pickedBooks.set(library, [book]);
        }
        
        // keep track of the already picked books
        db.alreadyPickedIds.push(index);
      }
    } else {
      // no books left, remove the library
      db.signedUpLibraries = db.signedUpLibraries.filter(
        l => l.id !== library.id
      );
    }
  }
};

/**
 * Format the relevant fields from the DB into output.
 * 
 * @returns {String}
 */
export const getFormattedOutput = () => {
  // first line is the amount of books
  let output = `${db.pickedBooks.size}\n`;

  // next print a section per library: library.id, number of books \n ids of books
  for (const [lib, books] of db.pickedBooks) {
    output += lib.id + ' ' + books.length + '\n';
    output += books.map(b => b.id).join(' ') + '\n';
  }

  return output;
};

/**
 * Cost/reward function used to sort the libraries and pick the best one.
 * 
 * @param {Number} daysUntilDeadline 
 * @param {Library} library 
 */
export const libraryReward = (daysUntilDeadline, library) => {
  // Here are some WIP algorithms
  // return library.books.size;
  // let totalScores = library.books.size !== 0 ? [...library.books.values()].reduce((acc, curr) => {
  //   return acc + curr.score
  // }, 0) : 1;
  // let booksSize = library.books.size === 0 ? 1 : library.books.size;
  // return totalScores / ((booksSize / library.maxBooksPerDay) * (5 * library.timeToSignUp));

  // how many 'working' (spent scanning) days the library has until deadline
  const workingDaysUntilDeadline = daysUntilDeadline - library.timeToSignUp;

  // how many days the scanning would take
  const productiveScanningDays = Math.ceil(
    library.books.size / library.maxBooksPerDay
  );

  const operationalTime = Math.min(
    workingDaysUntilDeadline,
    productiveScanningDays
  );

  // find out how many books we can manage to scan
  const numberOfBooksInTime = operationalTime * library.maxBooksPerDay;

  // get those books
  const feasibleBooksInLibrary = [...library.books.values()].slice(
    0,
    numberOfBooksInTime
  );

  // sum their scores
  const sumOfFeasibleBookScores = feasibleBooksInLibrary.reduce(
    (acc, curr) => acc + curr.score,
    0
  );

  /*
  The score is:
  - proportional to: 
    - total score of books we can mange to scan 
    - number of books in the library
  
  - in inverse proportion to:
    - the time to sign up
   */
  return (
    (sumOfFeasibleBookScores * library.books.size) / (library.timeToSignUp * 20)
  );
};
