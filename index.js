import { parseBooks } from './bookParser.js';
import { processBooks } from './engine.js';
import { writeToFile, printDbState } from './util.js';

/* 
 * Pick the right file depending on the argument
 * e.g. >>>node index.js b
 * runs the code on the b_read_on.txt file
 */
let inputFileName;
switch (process.argv[2]) {
  case 'b':
    inputFileName = 'b_read_on';
    break;
  case 'c':
    inputFileName = 'c_incunabula';
    break;
  case 'd':
    inputFileName = 'd_tough_choices';
    break;
  case 'e':
    inputFileName = 'e_so_many_books';
    break;
  case 'f':
    inputFileName = 'f_libraries_of_the_world';
    break;
  default:
    inputFileName = 'a_example';
}

/**
 * Main program.
 * Parses data, processes it, produces output.
 *
 * @param {string} fileName
 */
const main = async fileName => {
  console.time('Execution time');

  // parse/sort input data
  console.time('Parsing time');
  await parseBooks(inputFileName);
  console.timeEnd('Parsing time');

  console.log('\nParsing finished, db state:');
  printDbState();

  // main processing
  const output = await processBooks();
  console.timeEnd('Execution time');

  console.log('\nWriting to file, db state:');
  printDbState();
  await writeToFile(`./output/${inputFileName}.txt`, output);
};

main(inputFileName);
