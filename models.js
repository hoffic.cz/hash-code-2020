export class Book {
  id;
  score;

  constructor(id) {
    this.id = id;
  }
}

export class Library {
  id;
  numberOfBooks;
  books = new Map();
  timeToSignUp;
  maxBooksPerDay;

  constructor(id) {
    this.id = id;
  }
}