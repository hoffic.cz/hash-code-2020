import db from './db.js';
import { scanBooks, getFormattedOutput, libraryReward } from './util.js';

/**
 * Main event loop.
 */
export const processBooks = () =>
  new Promise(resolve => {
    let libraryReadyAtDay = 0;

    // sort the libraries initially according to the reward
    let daysUntilDeadline = db.numDays;
    let sortedLibraries = [...db.libraries.values()].sort(
      (a, b) =>
        libraryReward(daysUntilDeadline, b) -
        libraryReward(daysUntilDeadline, a)
    );

    // grab the first one
    let libraryBeingSignedUp = sortedLibraries.shift();

    // main event loop - while there are days left
    while (db.currentDay <= db.numDays) {
      // scan books from the signed up libraries
      for (const lib of db.signedUpLibraries) {
        scanBooks(lib);
      }

      // if the library which is currently being signed up is ready
      if (libraryReadyAtDay <= db.currentDay) {
        // finish signing up a library
        db.signedUpLibraries.push(libraryBeingSignedUp);

        // stop if no more libraries left
        if (sortedLibraries.length === 0) {
          break;
        }

        // grab another one
        libraryBeingSignedUp = sortedLibraries.shift();
        libraryReadyAtDay = db.currentDay + libraryBeingSignedUp.timeToSignUp;
      }

      // sort the left-over libraries every 100 days 
      // ideally we would want to do that every day but it's too inefficient
      if (db.currentDay % 100 === 0) {
        daysUntilDeadline = db.numDays - db.currentDay;
        sortedLibraries = sortedLibraries.sort(
          (a, b) =>
            libraryReward(daysUntilDeadline, b) -
            libraryReward(daysUntilDeadline, a)
        );
      }

      db.currentDay++;
    }

    resolve(getFormattedOutput());
  });
