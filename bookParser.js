import { processLineByLine } from './parser.js';
import db from './db.js';
import { Book, Library } from './models.js';
import { libraryReward } from './util.js';

export const parseBooks = async fileName => {
  return await processLineByLine(fileName, (line, index, stop) => {
    // params line
    if (index === 0) {
      const params = line.split(' ').map(s => +s); // parse to nums
      [db.numBooks, db.numLibraries, db.numDays] = params;

      // init books
      let books = new Array(db.numBooks).fill(undefined).map((el, index) => [index, new Book(index)]);
      db.books = new Map(books);

      // init libraries
      let libraries = new Array(db.numLibraries)
        .fill(undefined)
        .map((el, index) => [index, new Library(index)]);
      db.libraries = new Map(libraries);
    } 

    // scores line
    if (index === 1) {
      const bookScores = line.split(' ').map(s => +s); //parse to nums
      for (let i = 0; i < bookScores.length; i++) {
        // set scores of books
        db.books.get(i).score = bookScores[i];
      }
    }

    if (index > 1 && index <= (2 * db.numLibraries)) {
      let libIndex = Math.floor((index - 2) / 2);
      // first line 
      if (index % 2 === 0) {
        const libParams = line.split(' ').map(s => +s); // parse to nums
        let [numBooks, numDays, perDay] = libParams;
        db.libraries.get(libIndex).numberOfBooks = numBooks;
        db.libraries.get(libIndex).timeToSignUp = numDays;
        db.libraries.get(libIndex).maxBooksPerDay = perDay;
      } else {
        const bookIds = line.split(' ').map(s => +s); // parse to nums
        for (let i = 0; i < bookIds.length; i++) {
          let bookId = bookIds[i];
          // add books from global map to the library books
          db.libraries.get(libIndex).books.set(bookId, db.books.get(bookId));
        }
        db.libraries.get(libIndex).books = new Map(
          [...db.libraries.get(libIndex).books].sort((a,b) => b[1].score - a[1].score)
        );
      }
    }
  });
};
