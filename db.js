let numLibraries;
let numBooks;
let currentDay = 0;
let numDays;

let books;
let libraries;
let signedUpLibraries = [];

// library => [books]
let pickedBooks = new Map();
let alreadyPickedIds = [];

export default {
  numLibraries,
  numBooks,
  numDays,
  currentDay,
  books,
  libraries,
  signedUpLibraries,
  pickedBooks,
  alreadyPickedIds
}