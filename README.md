## Solution to the Google Hash Code Competition 2020

### #hashcodesolved

### https://hashcode.withgoogle.com

## Problem statement

#### [PDF](./hashcode_2020_online_qualification_round.pdf)

## Algorithm

While there are days left, our algorithm scans books from the already signed up libraries and picks the best possible library to start signing up according to a reward function.

The code achieved the score of 25,087,941 points, placing us:

- 2nd in our local hub (off by 300k points :( ),
- 2044th in the world

### Usage

Dependencies:

- Node.js 13.9.0
- Prettier (only used for quick formatting)

```
node index.js (a|b|c|d|e)
```

## Score

| Input                                                              |    Points |
| ------------------------------------------------------------------ | --------: |
| [A - example](./input/a_example.txt)                               |        17 |
| [B - read on](./input/b_read_on.txt)                               | 5,727,800 |
| [C - incunabula](./input/c_incunabula.txt)                         | 5,638,203 |
| [D - tough choices](./input/d_tough_choices.txt)                   | 4,815,395 |
| [E - so many books](./input/e_so_many_books.txt)                   | 4,177,367 |
| [F - libraries of the world](./input/f_libraries_of_the_world.txt) | 4,729,159 |

       Total score: 25,087,941

## Authors

#### Krzysztof Bielikowicz [@kbielikowicz98](https://gitlab.com/kbielikowicz98)

#### Mateusz Meller [@Matbanero](https://gitlab.com/Matbanero)

#### Petr Hoffmann [@hoffic.cz](https://gitlab.com/hoffic.cz)
