import { createInterface } from 'readline';
import { createReadStream } from 'fs';
import { once } from 'events';

/**
 * Execute a callback for each line in the given file.
 *
 * @param {string} fileName
 * @param {function} callback
 */
export const processLineByLine = (fileName, callback) =>
  new Promise(resolve => {
    const fileStream = createReadStream(`./input/${fileName}.txt`);
    const rl = createInterface({
      input: fileStream,
      crlfDelay: Infinity
    });

    let index = 0;
    // execute a callback for each line
    rl.on('line', line => {
      // pass in the line, its index and a callback to stop reading
      callback(line, index, resolve);
      index++;
    });

    rl.on('close', resolve);
  });
